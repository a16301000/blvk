package com.example.blvk;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.blvk.Adaptadores.AdaptadorIdiomas;
import com.example.blvk.Adaptadores.AdaptadorPremios;
import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Idioma;
import com.example.blvk.Data.Entidades.Premio;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static com.example.blvk.R.string.añadir_idioma;
import static com.example.blvk.R.string.añadir_premio;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentIdioma#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentIdioma extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentIdioma() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentIdioma.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentIdioma newInstance(String param1, String param2) {
        FragmentIdioma fragment = new FragmentIdioma();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_idioma, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }

    private void init(@NotNull View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Idioma> idiomas = dataManager.getAllIdiomas();
        AdaptadorIdiomas adaptadorIdiomas = new AdaptadorIdiomas(idiomas,getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.idiomasRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adaptadorIdiomas);

        FloatingActionButton fab = view.findViewById(R.id.botonNuevoIdioma);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, añadir_idioma, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action, null).show();

                Navigation.findNavController(view).navigate(R.id.fragmentEdicionIdioma);

            }
        });
    }
}