package com.example.blvk.Data.Entidades;

public class Pelicula {

    private int id;
    private String nombrePelicula;
    private int añoPelicula;
    private int idDirector;
    private int idGenero;
    private int idIdioma;
    private int idClasificacion;
    private int idPremio;

    public Pelicula(int id, String nombrePelicula, int añoPelicula, int idDirector, int idGenero, int idIdioma, int idClasificacion, int idPremio) {
        this.id = id;
        this.nombrePelicula = nombrePelicula;
        this.añoPelicula = añoPelicula;
        this.idDirector = idDirector;
        this.idGenero = idGenero;
        this.idIdioma = idIdioma;
        this.idClasificacion = idClasificacion;
        this.idPremio = idPremio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public void setNombrePelicula(String nombrePelicula) {
        this.nombrePelicula = nombrePelicula;
    }

    public int getAño() {
        return añoPelicula;
    }

    public void setAño(int año) {
        this.añoPelicula = año;
    }

    public int getIdDirector() {
        return idDirector;
    }

    public void setIdDirector(int idDirector) {
        this.idDirector = idDirector;
    }

    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    public int getIdIdioma() {
        return idIdioma;
    }

    public void setIdIdioma(int idIdioma) {
        this.idIdioma = idIdioma;
    }

    public int getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(int idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public int getIdPremio() {
        return idPremio;
    }

    public void setIdPremio(int idPremio) {
        this.idPremio = idPremio;
    }
}
