package com.example.blvk.Data.Entidades;

public class Clasificacion {

    private int id;
    private String descripcionClasificacion;

    public Clasificacion(int id, String descripcionClasificacion) {
        this.id = id;
        this.descripcionClasificacion = descripcionClasificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcionClasificacion() {
        return descripcionClasificacion;
    }

    public void setDescripcionClasificacion(String descripcionClasificacion) {
        this.descripcionClasificacion = descripcionClasificacion;
    }
}
