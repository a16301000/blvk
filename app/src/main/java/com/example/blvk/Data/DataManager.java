package com.example.blvk.Data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.blvk.Data.Entidades.Clasificacion;
import com.example.blvk.Data.Entidades.Director;
import com.example.blvk.Data.Entidades.Genero;
import com.example.blvk.Data.Entidades.Idioma;
import com.example.blvk.Data.Entidades.Pelicula;
import com.example.blvk.Data.Entidades.Premio;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DataManager {

    //Tabla Directores
    private static final String TABLA_DIRECTORES = "directores";
    private static final String TABLA_DIRECTORES_ID = "id_director";
    private static final String TABLA_DIRECTORES_NOMBRE = "nombre";
    private static final String TABLA_DIRECTORES_APELLIDO = "apellido";

    //Tabla Peliculas
    private static final String TABLA_PELICULAS = "peliculas";
    private static final String TABLA_PELICULAS_ID = "id_pelicula";
    private static final String TABLA_PELICULAS_NOMBRE = "nombre_pelicula";
    private static final String TABLA_PELICULAS_AÑO = "año";
    //FOREIGN KEYs
    private static final String TABLA_PELICULAS_ID_DIRECTOR = "id_director";
    private static final String TABLA_PELICULAS_ID_GENERO = "id_genero";
    private static final String TABLA_PELICULAS_ID_IDIOMA = "id_idioma";
    private static final String TABLA_PELICULAS_ID_CLASIFICACION = "id_clasificacion";
    private static final String TABLA_PELICULAS_ID_PREMIO = "id_premio";

    //Tabla Generos
    private static final String TABLA_GENEROS = "generos";
    private static final String TABLA_GENEROS_ID = "id_genero";
    private static final String TABLA_GENEROS_NOMBRE = "nombre_genero";

    //Tabla Idiomas
    private static final String TABLA_IDIOMAS = "idiomas";
    private static final String TABLA_IDIOMAS_ID = "id_idioma";
    private static final String TABLA_IDIOMAS_NOMBRE = "nombre_idioma";

    //Tabla Clasificaciones
    private static final String TABLA_CLASIFICACIONES = "clasificaciones";
    private static final String TABLA_CLASIFICACIONES_ID = "id_clasificacion";
    private static final String TABLA_CLASIFICACIONES_DESCRIPCION = "descripcion_clasificacion";

    //Tabla Premios
    private static final String TABLA_PREMIOS = "premios";
    private static final String TABLA_PREMIOS_ID = "id_premio";
    private static final String TABLA_PREMIOS_NOMBRE = "nombre_premio";

    private static final String BD_NOMBRE = "cine.db";
    private static final int BD_VERSION = 3;
    private static String DB_PATH;
    //Base de datos
    private SQLiteDatabase db;

    public DataManager(Context context) {

        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        String FULL_PATH = DB_PATH + BD_NOMBRE;
        File file = new File(FULL_PATH);
        if (!file.exists()) {
            CustomSQLiteOpenHelper helper = new
                    CustomSQLiteOpenHelper(context);

            db = helper.getWritableDatabase();
            this.copyDatabase(context);
        }
        Log.i("Info", String.format("DB in: %s", FULL_PATH));

        CustomSQLiteOpenHelper helper = new
                CustomSQLiteOpenHelper(context);

        db = helper.getWritableDatabase();

    }

    public void copyDatabase(Context context) {
        try {
            for (String asset : context.getAssets().list("")) {
            }
            InputStream inputStream = context.getAssets().open(BD_NOMBRE);

            String outFileName = DB_PATH + BD_NOMBRE;

            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];

            int lenght;
            int total = 0;

            while ((lenght = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, lenght);
                total += lenght;
            }
            inputStream.close();
            outputStream.flush();
            outputStream.close();
            Log.i("Escrito: ", String.valueOf(total));

        } catch (Exception e) {
            Log.i("Error copy db: ", e.getMessage());
        }
    }

    //______________________________________________________________________________________________________________________________________________
    //Opciones de Director
    public boolean addDirector(@NotNull Director director) {
        String query = String.format("insert into %s(%s,%s) values (?,?);", TABLA_DIRECTORES, TABLA_DIRECTORES_NOMBRE, TABLA_DIRECTORES_APELLIDO);

        String[] arguments = new String[]{
                director.getNombre(),
                director.getApellido(),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Director getDirector(int id) {
        if (id > 0) {
            Director director;
            String query = String.format("select * from %s where %s = ?", TABLA_DIRECTORES, TABLA_DIRECTORES_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            director = new Director(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
            );
            return director;
        } else
            return null;
    }

    public ArrayList<Director> getAllDirectors() {
        ArrayList<Director> directors = new ArrayList<Director>();
        String query = String.format("select * from %s", TABLA_DIRECTORES);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Director director = new Director(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2));
                directors.add(director);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return directors;
    }

    public boolean editDirector(@NotNull Director director) {
        String query = String.format("update %s set %s=?,%s=? where %s = ?", TABLA_DIRECTORES, TABLA_DIRECTORES_NOMBRE, TABLA_DIRECTORES_APELLIDO, TABLA_DIRECTORES_ID);
        Log.i("prueba", query);
        String[] arguments = new String[]{
                director.getNombre(),
                director.getApellido(),
                String.valueOf(director.getId())
        };
        db.execSQL(query, arguments);
        return true;
    }

    public void deleteDirector(@NotNull Director director) {
        db.delete(TABLA_DIRECTORES, String.format("%s = ?", TABLA_DIRECTORES_ID), new String[]{String.valueOf(director.getId())});
        db.close();
    }
    //______________________________________________________________________________________________________________________________________________
    //Opciones de Pelicula

    public boolean addPelicula(@NotNull Pelicula pelicula) {
        String query = String.format("insert into %s(%s,%s,%s,%s,%s,%s,%s) values (?,?,?,?,?,?,?);", TABLA_PELICULAS, TABLA_PELICULAS_NOMBRE, TABLA_PELICULAS_AÑO, TABLA_PELICULAS_ID_DIRECTOR, TABLA_PELICULAS_ID_GENERO, TABLA_PELICULAS_ID_IDIOMA, TABLA_PELICULAS_ID_CLASIFICACION, TABLA_PELICULAS_ID_PREMIO);

        String[] arguments = new String[]{
                pelicula.getNombrePelicula(),
                String.valueOf(pelicula.getAño()),
                String.valueOf(pelicula.getIdDirector()),
                String.valueOf(pelicula.getIdGenero()),
                String.valueOf(pelicula.getIdIdioma()),
                String.valueOf(pelicula.getIdClasificacion()),
                String.valueOf(pelicula.getIdPremio()),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Pelicula getPelicula(int id) {
        if (id > 0) {
            Pelicula pelicula;
            String query = String.format("select * from %s where %s = ?", TABLA_PELICULAS, TABLA_PELICULAS_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            pelicula = new Pelicula(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getInt(5),
                    cursor.getInt(6),
                    cursor.getInt(7)
            );
            return pelicula;
        } else
            return null;
    }

    public ArrayList<Pelicula> getAllPeliculas() {
        ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
        String query = String.format("select * from %s", TABLA_PELICULAS);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Pelicula pelicula = new Pelicula(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getInt(2),
                        cursor.getInt(3),
                        cursor.getInt(4),
                        cursor.getInt(5),
                        cursor.getInt(6),
                        cursor.getInt(7));
                peliculas.add(pelicula);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return peliculas;
    }

    public boolean editPelicula(@NotNull Pelicula pelicula) {
        String query = String.format("update %s set %s=?,%s=?,%s=?,%s=?,%s=?,%s=?,%s=? where %s = ?", TABLA_PELICULAS, TABLA_PELICULAS_NOMBRE, TABLA_PELICULAS_AÑO, TABLA_PELICULAS_ID_DIRECTOR, TABLA_PELICULAS_ID_GENERO, TABLA_PELICULAS_ID_IDIOMA, TABLA_PELICULAS_ID_CLASIFICACION, TABLA_PELICULAS_ID_PREMIO, TABLA_PELICULAS_ID);
        String[] arguments = new String[]{
                pelicula.getNombrePelicula(),
                String.valueOf(pelicula.getAño()),
                String.valueOf(pelicula.getIdDirector()),
                String.valueOf(pelicula.getIdGenero()),
                String.valueOf(pelicula.getIdIdioma()),
                String.valueOf(pelicula.getIdClasificacion()),
                String.valueOf(pelicula.getIdPremio()),
                String.valueOf(pelicula.getId()),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public boolean deletePelicula(@NotNull Pelicula pelicula) {
        db.enableWriteAheadLogging();
        db.delete(TABLA_PELICULAS, String.format("%s = ?", TABLA_PELICULAS_ID), new String[]{String.valueOf(pelicula.getId())});
        db.disableWriteAheadLogging();
        db.close();
        return true;
    }
    //______________________________________________________________________________________________________________________________________________
    //Opciones de Genero

    public boolean addGenero(@NotNull Genero genero) {
        String query = String.format("insert into %s(%s) values (?);", TABLA_GENEROS, TABLA_GENEROS_NOMBRE);

        String[] arguments = new String[]{
                genero.getNombreGenero(),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Genero getGenero(int id) {
        if (id > 0) {
            Genero genero;
            String query = String.format("select * from %s where %s = ?", TABLA_GENEROS, TABLA_GENEROS_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            genero = new Genero(cursor.getInt(0),
                    cursor.getString(1)
            );
            return genero;
        } else
            return null;
    }

    public ArrayList<Genero> getAllGeneros() {
        ArrayList<Genero> generos = new ArrayList<Genero>();
        String query = String.format("select * from %s", TABLA_GENEROS);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Genero genero = new Genero(
                        cursor.getInt(0),
                        cursor.getString(1));
                generos.add(genero);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return generos;
    }

    public boolean editGenero(@NotNull Genero genero) {
        String query = String.format("update %s set %s=? where %s = ?", TABLA_GENEROS, TABLA_GENEROS_NOMBRE, TABLA_GENEROS_ID);
        Log.i(" query", query);
        Log.i("genero", genero.getNombreGenero());
        String[] arguments = new String[]{
                genero.getNombreGenero(),
                String.valueOf(genero.getId())
        };
        db.execSQL(query, arguments);
        return true;
    }

    public boolean deleteGenero(@NotNull Genero genero) {
        db.enableWriteAheadLogging();
        db.delete(TABLA_GENEROS, String.format("%s = ?", TABLA_GENEROS_ID), new String[]{String.valueOf(genero.getId())});
        db.disableWriteAheadLogging();
        db.close();
        return true;
    }

    //______________________________________________________________________________________________________________________________________________
    //Opciones de Idioma

    public boolean addIdioma(@NotNull Idioma idioma) {
        String query = String.format("insert into %s(%s) values (?);", TABLA_IDIOMAS, TABLA_IDIOMAS_NOMBRE);

        String[] arguments = new String[]{
                idioma.getNombreIdioma(),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Idioma getIdioma(int id) {
        if (id > 0) {
            Idioma idioma;
            String query = String.format("select * from %s where %s = ?", TABLA_IDIOMAS, TABLA_IDIOMAS_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            idioma = new Idioma(cursor.getInt(0),
                    cursor.getString(1)
            );
            return idioma;
        } else
            return null;
    }

    public ArrayList<Idioma> getAllIdiomas() {
        ArrayList<Idioma> idiomas = new ArrayList<Idioma>();
        String query = String.format("select * from %s", TABLA_IDIOMAS);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Idioma idioma = new Idioma(
                        cursor.getInt(0),
                        cursor.getString(1));
                idiomas.add(idioma);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return idiomas;
    }

    public boolean editIdioma(@NotNull Idioma idioma) {
        String query = String.format("update %s set %s=? where %s = ?", TABLA_IDIOMAS, TABLA_IDIOMAS_NOMBRE, TABLA_IDIOMAS_ID);
        String[] arguments = new String[]{
                idioma.getNombreIdioma(),
                String.valueOf(idioma.getId())
        };
        db.execSQL(query, arguments);
        return true;
    }

    public boolean deleteIdioma(@NotNull Idioma idioma) {
        db.enableWriteAheadLogging();
        db.delete(TABLA_IDIOMAS, String.format("%s = ?", TABLA_IDIOMAS_ID), new String[]{String.valueOf(idioma.getId())});
        db.disableWriteAheadLogging();
        db.close();
        return true;
    }

    //______________________________________________________________________________________________________________________________________________
    //Opciones de Clasificacion

    public boolean addClasificacion(@NotNull Clasificacion clasificacion) {
        String query = String.format("insert into %s(%s) values (?);", TABLA_CLASIFICACIONES, TABLA_CLASIFICACIONES_DESCRIPCION);

        String[] arguments = new String[]{
                clasificacion.getDescripcionClasificacion(),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Clasificacion getClasificacion(int id) {
        if (id > 0) {
            Clasificacion clasificacion;
            String query = String.format("select * from %s where %s = ?", TABLA_CLASIFICACIONES, TABLA_CLASIFICACIONES_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            clasificacion = new Clasificacion(cursor.getInt(0),
                    cursor.getString(1)
            );
            return clasificacion;
        } else
            return null;
    }

    public ArrayList<Clasificacion> getAllClasificaciones() {
        ArrayList<Clasificacion> clasificacions = new ArrayList<Clasificacion>();
        String query = String.format("select * from %s", TABLA_CLASIFICACIONES);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Clasificacion clasificacion = new Clasificacion(
                        cursor.getInt(0),
                        cursor.getString(1));
                clasificacions.add(clasificacion);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return clasificacions;
    }

    public boolean editClasificacion(@NotNull Clasificacion clasificacion) {
        String query = String.format("update %s set %s=? where %s = ?", TABLA_CLASIFICACIONES, TABLA_CLASIFICACIONES_DESCRIPCION, TABLA_CLASIFICACIONES_ID);
        String[] arguments = new String[]{
                clasificacion.getDescripcionClasificacion(),
                String.valueOf(clasificacion.getId())
        };
        db.execSQL(query, arguments);
        return true;
    }

    public boolean deleteClasificacion(@NotNull Clasificacion clasificacion) {
        db.enableWriteAheadLogging();
        db.delete(TABLA_CLASIFICACIONES, String.format("%s = ?", TABLA_CLASIFICACIONES_ID), new String[]{String.valueOf(clasificacion.getId())});
        db.disableWriteAheadLogging();
        db.close();
        return true;
    }

    //______________________________________________________________________________________________________________________________________________
    //Opciones de Premio

    public boolean addPremio(@NotNull Premio premio) {
        String query = String.format("insert into %s(%s) values (?);", TABLA_PREMIOS, TABLA_PREMIOS_NOMBRE);

        String[] arguments = new String[]{
                premio.getNombrePremio(),
        };
        db.execSQL(query, arguments);
        return true;
    }

    public Premio getPremio(int id) {
        if (id > 0) {
            Premio premio;
            String query = String.format("select * from %s where %s = ?", TABLA_PREMIOS, TABLA_PREMIOS_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Se mueve a la primera posición
            cursor.moveToFirst();
            premio = new Premio(cursor.getInt(0),
                    cursor.getString(1)
            );
            return premio;
        } else
            return null;
    }

    public ArrayList<Premio> getAllPremios() {
        ArrayList<Premio> premios = new ArrayList<Premio>();
        String query = String.format("select * from %s", TABLA_PREMIOS);
        try (Cursor cursor = db.rawQuery(query, null)) {
            // Move to first row
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Premio premio = new Premio(
                        cursor.getInt(0),
                        cursor.getString(1));
                premios.add(premio);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
        }
        return premios;
    }

    public boolean editPremio(@NotNull Premio premio) {
        String query = String.format("update %s set %s=? where %s = ?", TABLA_PREMIOS, TABLA_PREMIOS_NOMBRE, TABLA_PREMIOS_ID);
        Log.i("prueba", query);
        String[] arguments = new String[]{
                premio.getNombrePremio(),
                String.valueOf(premio.getId())
        };
        db.execSQL(query, arguments);
        return true;
    }

    public boolean deletePremio(@NotNull Premio premio) {
        db.enableWriteAheadLogging();
        db.delete(TABLA_PREMIOS, String.format("%s = ?", TABLA_PREMIOS_ID), new String[]{String.valueOf(premio.getId())});
        db.disableWriteAheadLogging();
        db.close();
        return true;
    }

    //______________________________________________________________________________________________________________________________________________


    private class CustomSQLiteOpenHelper extends SQLiteOpenHelper {
        Context context;

        public CustomSQLiteOpenHelper(Context context) {
            super(context, BD_NOMBRE, null, BD_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}