package com.example.blvk.Data.Entidades;

public class Premio {

    private int id;
    private String nombrePremio;

    public Premio(Integer id, String nombrePremio) {
        this.id = id;
        this.nombrePremio = nombrePremio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrePremio() {
        return nombrePremio;
    }

    public void setNombrePremio(String nombrePremio) {
        this.nombrePremio = nombrePremio;
    }
}