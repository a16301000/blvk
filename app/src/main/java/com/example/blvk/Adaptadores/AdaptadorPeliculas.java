package com.example.blvk.Adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Pelicula;
import com.example.blvk.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdaptadorPeliculas extends RecyclerView.Adapter<AdaptadorPeliculas.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Pelicula> mdata;

    public AdaptadorPeliculas(List<Pelicula> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public AdaptadorPeliculas.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.card_peliculas,null);
        return new AdaptadorPeliculas.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final AdaptadorPeliculas.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Pelicula> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        TextView primaryText; //Pelicula
        TextView secundaryText; //Año
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            primaryText = itemView.findViewById(R.id.nombrePeliculaCardView);
            secundaryText = itemView.findViewById(R.id.añoCardView);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Pelicula pelicula) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(pelicula.getNombrePelicula());
            secundaryText.setText(String.valueOf(pelicula.getAño()));

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) pelicula.getId();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.fragmentEdicionPelicula,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataManager.deletePelicula(pelicula);
                    mdata.remove(pelicula);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
