package com.example.blvk.Adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Genero;
import com.example.blvk.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdaptadorGeneros extends RecyclerView.Adapter<AdaptadorGeneros.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Genero> mdata;

    public AdaptadorGeneros(List<Genero> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public AdaptadorGeneros.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.card_generos,null);
        return new AdaptadorGeneros.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final AdaptadorGeneros.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Genero> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        TextView primaryText; //Genero
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            primaryText = itemView.findViewById(R.id.nombreGeneroCardView);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Genero genero) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(genero.getNombreGenero());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) genero.getId();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.fragmentEdicionGenero,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataManager.deleteGenero(genero);
                    mdata.remove(genero);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
