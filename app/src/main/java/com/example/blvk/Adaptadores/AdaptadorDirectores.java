package com.example.blvk.Adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Director;
import com.example.blvk.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdaptadorDirectores extends RecyclerView.Adapter<AdaptadorDirectores.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Director> mdata;

    public AdaptadorDirectores(List<Director> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public AdaptadorDirectores.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.card_directores,null);
        return new AdaptadorDirectores.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final AdaptadorDirectores.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Director> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        ImageView avatar;
        TextView primaryText, secondaryText; //Author
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.fotoDirector);
            primaryText = itemView.findViewById(R.id.nombreDirectorCardView);
            secondaryText = itemView.findViewById(R.id.apellidoDirectorCardView);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Director director) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(director.getNombre());
            secondaryText.setText(director.getApellido());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) director.getId();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.fragmentEdicionDirector,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataManager.deleteDirector(director);
                    mdata.remove(director);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
