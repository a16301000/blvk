package com.example.blvk.Adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Idioma;
import com.example.blvk.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdaptadorIdiomas extends RecyclerView.Adapter<AdaptadorIdiomas.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Idioma> mdata;

    public AdaptadorIdiomas(List<Idioma> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public AdaptadorIdiomas.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.card_idiomas,null);
        return new AdaptadorIdiomas.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final AdaptadorIdiomas.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Idioma> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        TextView primaryText; //Idioma
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            primaryText = itemView.findViewById(R.id.nombreIdiomaCardView);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Idioma idioma) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(idioma.getNombreIdioma());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) idioma.getId();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.fragmentEdicionIdioma,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataManager.deleteIdioma(idioma);
                    mdata.remove(idioma);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
