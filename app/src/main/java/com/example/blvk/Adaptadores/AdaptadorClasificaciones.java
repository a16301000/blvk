package com.example.blvk.Adaptadores;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Clasificacion;
import com.example.blvk.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdaptadorClasificaciones extends RecyclerView.Adapter<AdaptadorClasificaciones.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Clasificacion> mdata;

    public AdaptadorClasificaciones(List<Clasificacion> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public AdaptadorClasificaciones.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.card_clasificaciones,null);
        return new AdaptadorClasificaciones.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final AdaptadorClasificaciones.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Clasificacion> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        TextView primaryText; //Clasificacion
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            primaryText = itemView.findViewById(R.id.nombreClasificacionCardView);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Clasificacion clasificacion) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(clasificacion.getDescripcionClasificacion());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) clasificacion.getId();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.fragmentEdicionClasificacion,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dataManager.deleteClasificacion(clasificacion);
                    mdata.remove(clasificacion);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
