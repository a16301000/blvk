package com.example.blvk.Edicion;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.blvk.Adaptadores.AdaptadorDirectores;
import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Director;
import com.example.blvk.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import static com.example.blvk.R.string.añadir_director;
import com.example.blvk.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEdicionDirector#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEdicionDirector extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentEdicionDirector() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentEdicionDirector.
     */
    // TODO: Rename and change types and number of parameters
    @NotNull
    public static FragmentEdicionDirector newInstance(String param1, String param2) {
        FragmentEdicionDirector fragment = new FragmentEdicionDirector();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edicion_director, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init (@NotNull View view){

        final DataManager dataManager = new DataManager(requireActivity());

        //Getting data from bundle
        int id = 0;

        if(requireArguments() != null)
            id = (int) requireArguments().getFloat("id");
        Log.i("argument", String.valueOf(id));

        //Declare Data ModelTo fill
        Director director = dataManager.getDirector(id);

        //Text Fields
        TextInputLayout textFieldNombre = requireActivity().findViewById(R.id.textFieldNombre);
        TextInputLayout textFieldApellido = requireActivity().findViewById(R.id.textFieldApellido);

        //Autocompletar datos
        if (id > 0 && director != null) {

            //Text views
            Objects.requireNonNull(textFieldNombre.getEditText()).setText(director.getNombre());
            Objects.requireNonNull(textFieldApellido.getEditText()).setText(director.getApellido());
        }

        //Botones Aceptar y Cancelar
        Button guardarButtonDirector = requireActivity().findViewById(R.id.boton_guardar);
        Button cancelarButtonDirector = requireActivity().findViewById(R.id.boton_cancelar);

        //Verificacion
        cancelarButtonDirector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.fragmentDirector);
            }
        });

        int finalId = id;
        guardarButtonDirector.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Objects.requireNonNull(textFieldNombre.getEditText()).setError(null);
                Objects.requireNonNull(textFieldApellido.getEditText()).setError(null);
                if (textFieldNombre.getEditText().getText().toString().equals("") || textFieldApellido.getEditText().getText().toString().equals("")) {
                    Snackbar.make(view, R.string.error_total, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Here we add the new person
                Director director = new Director(finalId,
                        textFieldNombre.getEditText().getText().toString(),
                        textFieldApellido.getEditText().getText().toString()
                );
                Log.i(String.valueOf(R.string.action), director.getNombre());
                Log.i(String.valueOf(R.string.action), director.getApellido());
                if (finalId > 0)
                    dataManager.editDirector(director);
                else
                    dataManager.addDirector(director);
                Navigation.findNavController(view).navigate(R.id.fragmentDirector);
            }
        });

    }
}