package com.example.blvk.Edicion;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Director;
import com.example.blvk.Data.Entidades.Pelicula;
import com.example.blvk.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEdicionPelicula#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEdicionPelicula extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentEdicionPelicula() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentEdicionPelicula.
     */
    // TODO: Rename and change types and number of parameters
    @NotNull
    public static FragmentEdicionPelicula newInstance(String param1, String param2) {
        FragmentEdicionPelicula fragment = new FragmentEdicionPelicula();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edicion_pelicula, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init (View view){

        final DataManager dataManager = new DataManager(requireActivity());

        //Getting data from bundle
        int id = 0;

        if(requireArguments() != null)
            id = (int) requireArguments().getFloat("id");
        Log.i("argument", String.valueOf(id));

        //Declare Data ModelTo fill
        Pelicula pelicula = dataManager.getPelicula(id);

        //Text Fields
        TextInputLayout textFieldNombrePelicula = requireActivity().findViewById(R.id.textFieldNombrePelicula);
        TextInputLayout textFieldAñoPelicula = requireActivity().findViewById(R.id.textFieldAñoPelicula);

        //Autocompletar datos
        if (id > 0 && pelicula != null) {

            //Text views
            Objects.requireNonNull(textFieldNombrePelicula.getEditText()).setText(pelicula.getNombrePelicula());
            Objects.requireNonNull(textFieldAñoPelicula.getEditText()).setText(String.valueOf(pelicula.getAño()));
        }

        //Botones Aceptar y Cancelar
        Button guardarButtonPelicula = requireActivity().findViewById(R.id.boton_guardar);
        Button cancelarButtonPelicula = requireActivity().findViewById(R.id.boton_cancelar);

        //Verificacion
        cancelarButtonPelicula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.fragmentPelicula);
            }
        });

        int finalId = id;
        guardarButtonPelicula.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Objects.requireNonNull(textFieldNombrePelicula.getEditText()).setError(null);
                Objects.requireNonNull(textFieldAñoPelicula.getEditText()).setError(null);
                if (textFieldNombrePelicula.getEditText().getText().toString().equals("") || textFieldAñoPelicula.getEditText().getText().toString().equals("")) {
                    Snackbar.make(view, R.string.error_total, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Here we add the new person
                Pelicula pelicula = new Pelicula(finalId,
                        textFieldNombrePelicula.getEditText().getText().toString(),
                        Integer.parseInt(textFieldAñoPelicula.getEditText().getText().toString()),
                        finalId,
                        finalId,
                        finalId,
                        finalId,
                        finalId
                );
                Log.i(String.valueOf(R.string.action), pelicula.getNombrePelicula());
                Log.i(String.valueOf(R.string.action), String.valueOf(pelicula.getAño()));
                if (finalId > 0)
                    dataManager.editPelicula(pelicula);
                else
                    dataManager.addPelicula(pelicula);
                Navigation.findNavController(view).navigate(R.id.fragmentPelicula);
                //Hace falta rectificar este apartado
            }
        });
    }
}