package com.example.blvk.Edicion;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.blvk.Data.DataManager;
import com.example.blvk.Data.Entidades.Director;
import com.example.blvk.Data.Entidades.Premio;
import com.example.blvk.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentEdicionPremios#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentEdicionPremios extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentEdicionPremios() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentEdicionPremios.
     */
    // TODO: Rename and change types and number of parameters
    @NotNull
    public static FragmentEdicionPremios newInstance(String param1, String param2) {
        FragmentEdicionPremios fragment = new FragmentEdicionPremios();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edicion_premios, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init (@NotNull View view){

        final DataManager dataManager = new DataManager(requireActivity());

        //Getting data from bundle
        int id = 0;

        if(requireArguments() != null)
            id = (int) requireArguments().getFloat("id");
        Log.i("argument", String.valueOf(id));

        //Declare Data ModelTo fill
        Premio premio = dataManager.getPremio(id);

        //Text Fields
        TextInputLayout textFieldNombrePremio = requireActivity().findViewById(R.id.textFieldNombrePremio);

        //Autocompletar datos
        if (id > 0 && premio != null) {

            //Text views
            Objects.requireNonNull(textFieldNombrePremio.getEditText()).setText(premio.getNombrePremio());
        }

        //Botones Aceptar y Cancelar
        Button guardarButtonPremio = requireActivity().findViewById(R.id.boton_guardar);
        Button cancelarButtonPremio = requireActivity().findViewById(R.id.boton_cancelar);

        //Verificacion
        cancelarButtonPremio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.fragmentPremios);
            }
        });

        int finalId = id;
        guardarButtonPremio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Objects.requireNonNull(textFieldNombrePremio.getEditText()).setError(null);
                if (textFieldNombrePremio.getEditText().getText().toString().equals("")) {
                    Snackbar.make(view, R.string.error_total, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Here we add the new person
                Premio premio = new Premio(finalId,
                        textFieldNombrePremio.getEditText().getText().toString()
                );
                Log.i(String.valueOf(R.string.action), premio.getNombrePremio());
                if (finalId > 0)
                    dataManager.editPremio(premio);
                else
                    dataManager.addPremio(premio);
                Navigation.findNavController(view).navigate(R.id.fragmentPremios);
            }
        });

    }
}